/* -----------------------------------------------------------------------------
 *
 *     Copyright (c) 2017  -  GEMALTO DEVELOPMENT - R&D
 *
 * -----------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * -----------------------------------------------------------------------------
 */
#import <Foundation/Foundation.h>
#import <ZoomAuthenticationHybrid/ZoomPublicApi.h>

typedef NS_ENUM(NSInteger, FaceCaptureErrorStatus) {

    /**
     Succeed
     */
    FaceCaptureErrorStatusSucceed = -1,
    /**
     Failed
     */
    FaceCaptureErrorStatusFailed = 0,
    /**
     Canceled
     */
    FaceCaptureErrorStatusCanceled = 1
};

/** Represents the various end states of a verification session */
typedef NS_ENUM(NSInteger, FaceCaptureVerificationStatus) {
    /**
     Verification succeed
     */
    FaceCaptureVerificationStatusUserProcessedSuccessfully = 0,
    /**
     Verification failed
     */
    FaceCaptureVerificationStatusUserNotProcessed,
    /**
     User cancelled before completing verification
     */
    FaceCaptureVerificationStatusFailedBecauseUserCancelled,
    /**
     Validation of the app token did not succeed
     */
    FaceCaptureVerificationStatusFailedBecauseAppTokenNotValid,
    /**
     Camera Permission Denied
     */
    FaceCaptureVerificationStatusFailedBecauseCameraPermissionDenied,
    /**
     Verification cancelled because device is in landscape mode
     */
    FaceCaptureVerificationStatusFailedBecauseOfLandscapeMode,
    /**
     Verification cancelled because device is in reverse portrait mode
     */
    FaceCaptureVerificationStatusFailedBecauseOfReversePortraitMode,
    /**
     Verification cancelled due to time out
     */
    FaceCaptureVerificationStatusFailedBecauseOfTimeout,
    /**
     Verification cancelled because encryption key invalid
     */
    FaceCaptureVerificationStatusFailedBecauseEncryptionKeyInvalid,
    /**
     Verification failed because of an unknown and unexpected error.
     */
    FaceCaptureVerificationStatusUnknownError,
    /**
     Lockout Error.
     */
    FaceCaptureVerificationLockoutStatusError,
    /**
     Verification Error.
     */
    FaceCaptureVerificationStatusError

};

/** Represents the status of the SDK */
typedef NS_ENUM(NSInteger, FaceCaptureSDKStatus) {
   
    /**
     Initialize was never attempted
     */
    FaceCaptureSDKStatusNeverInitialized = 0,
    /**
     The app token provided was verified
     */
    FaceCaptureSDKStatusInitialized,
    /**
     The app token could not be verified
     */
    FaceCaptureSDKStatusNetworkIssues,
    /**
     The app token provided was invalid
     */
    FaceCaptureSDKStatusInvalidToken,
    /**
     An unknown error occurred
     */
    FaceCaptureSDKStatusUnknownError,
    /**
     Device is in landscape mode
     */
    FaceCaptureSDKStatusDeviceInLandscapeMode,
    /**
     Device is in reverse portrait mode
     */
    FaceCaptureSDKStatusDeviceInReversePortraitMode,
    /**
     The provided license has expired or it contains invalid text.
     */
    FaceCaptureSDKStatusLicenseExpiredOrInvalid,
    /**
     Device is locked out due to too many failures
     */
    FaceCaptureSDKStatusLockoutStatusError,
    /**
     SDK error
     */
    FaceCaptureSDKStatusError
};

/** Represents the possible state of camera permissions. */
typedef NS_ENUM(NSInteger, FaceCaptureCameraPermissionStatus) {
    /** The user has not yet been asked for permission to use the camera */
     FaceCaptureCameraPermissionStatusNotDetermined = 0,
    /** The user denied the app permission to use the camera or manually revoked the app’s camera permission.
     From this state, permission can only be modified by the user from System ‘Settings’ context. */
     FaceCaptureCameraPermissionStatusDenied = 1,
    /** The camera permission on this device has been disabled due to policy.
     From this state, permission can only be modified by the user from System ‘Settings’ context or contacting the system administrator. */
     FaceCaptureCameraPermissionStatusRestricted = 2,
    /** The user granted permission to use the camera. */
     FaceCaptureCameraPermissionStatusAuthorized = 3,
};

@interface FaceCaptureStatusManager : NSObject

+ (NSString *) handleFaceCaptureVerificationStatus:(ZoomVerificationStatus)status;
+ (NSString *) handleFaceCaptureSDKStatus:(ZoomSDKStatus) status;
+ (NSString *) handleFaceCaptureCameraPermissiontatus:(ZoomCameraPermissionStatus) status;
+ (enum FaceCaptureErrorStatus) errorStatus;
+ (NSString *) verificationStateMessage:(enum FaceCaptureVerificationStatus)status;

@end
