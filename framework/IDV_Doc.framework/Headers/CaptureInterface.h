//
//  CaptureInterface.h
//  mcid_sdk
//
//  Copyright © 2018 Gemalto. All rights reserved.
//

#ifndef CAPTUREINTERFACE_H
#define CAPTUREINTERFACE_H

#import "mKYC_interface.h"
#import "Document.h"
#import "CaptureResult.h"


/**
 CaptureError
 */
typedef NS_ENUM(NSUInteger, CaptureError) {
    NoError,
    /** Detected rooted device */
    RootedDevice,
    /** Detected invalid architecture */
    InvalidArchitecture,
    /** App has gone to background */
    AppInBackground,
    /** SDK is already on initialization state */
    AlreadyOnInit,
    /** SDK can't initialize the Edges model */
    BadEdgeDetectionModelInitialization,
    /** SDK can't initialize the OCR model */
    BadOCRDetectionModelInitialization,
    /** SDK can't initialize the camera */
    BadCameraInitialization,
    /** SDK released while initializing */
    CancelInitialization,
};


/**
 CaptureStep
 */
typedef NS_ENUM(NSUInteger, CaptureStep) {
    Detecting,
    Cropping,
    ResultOK,
    ResultKO
};

/**
 HotspotMode
 */
typedef NS_ENUM(NSUInteger, HotspotMode) {
    Percentil = 0,
    Index = 1
};

/**
  - EdgesMode
  - */
typedef NS_ENUM(NSUInteger, EdgesMode) {
    ImageProcessing = 0,
    MachineLearning = 1
};

/**
 CaptureScreen 
 */
typedef NS_ENUM(NSUInteger, CaptureScreen) {
    InDetecting,
    OutDetecting,
    InCropping,
    OutCropping,
    InResultOK,
    OutResultOK,
    InResultKO,
    OutResultKO,
};

/**
 DetectionWarning
 */
typedef NS_OPTIONS(NSUInteger, DetectionWarning) {
    FocusInProgress = 1 << 0,
    FitDocument     = 1 << 1,
    Hotspot         = 1 << 2,
    LowContrast     = 1 << 3,
    LowLight        = 1 << 4,
    BWPhotocopy     = 1 << 5,
    Blur            = 1 << 6
};


/**
 Capture delagate
 */
@protocol CaptureDelegate <BaseCaptureDelegate>

/** Called when capture finish with success

 @param p_side1 image from side 1
 @param p_side2 image from side 2
 @param p_metaData information about the capture process
 */
- (void) onSuccess:(NSData*)p_side1 side:(NSData*)p_side2 metadata:(NSDictionary*)p_metaData;
@optional
- (void) onSuccess:(CaptureResult *) captureResult;
- (void) onError:(CaptureError)p_failureCode;
- (void) onScreenChanged:(CaptureScreen)screen;
@end


/**
  Use this protocol to listen detection checks changes from
  the engine
 */
@protocol DetectionWarningDelegate <NSObject>

/**
 Called when the engine detects some warnings in the detection process

 @param warnings A enum set with all warnings which are activated
 */
- (void)detectionWarnings:(DetectionWarning)warnings;
@end

/**
 Capture interface
 */
@interface CaptureInterface : mKYC_interface

/**
 Init

 @param onCompletion Completion block
 */
-(bool) initWithCompletion:(void (^)(BOOL, int))onCompletion;

/**
 Start the sdk

 @param p_captureDelegate this delegate it will be called when start process finish
 */
-(void) start:(id<CaptureDelegate>)p_captureDelegate;
-(void) hideUIElementsForStep:(CaptureStep)step;
-(void) setCaptureDocuments:(NSArray<Document*>*)p_documents;
-(void) setDetectionWarningsDelegate:(id<DetectionWarningDelegate>)p_qaDelegate;
-(void) setAutoSnapshot:(Boolean)value;
-(void) setAutoCropping:(Boolean)value;
-(void) setHotspotMode:(HotspotMode)mode;
-(void) setEdgesMode:(EdgesMode)mode;
-(void) setBWPhotocopyQAEnabled:(bool)enable;
-(void) releaseMemory;
- (NSString*) version;
+ (NSString*) version;

#pragma mark Trigger events
-(void) triggerSnapshotButton;
-(void) triggerValidateCropButton;
-(void) triggerCancelCropButton;
-(void) triggerValidateResultButton;
-(void) triggerValidateSwipeButton;

@end

#endif /* CAPTUREINTERFACE_H */
