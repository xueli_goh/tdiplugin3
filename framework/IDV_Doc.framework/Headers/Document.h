//
//  DocType.h
//  mcid_sdk
//
//  Copyright © 2018 Gemalto. All rights reserved.
//

#ifndef DOCUMENT_H
#define DOCUMENT_H

#import <Foundation/Foundation.h>

#define DOCUMENT_TD1 [[Document alloc] initWithWidth:  85.60f andHeight: 53.98f andNumPages: 2]
#define DOCUMENT_TD2 [[Document alloc] initWithWidth: 105.00f andHeight: 74.00f andNumPages: 2]
#define DOCUMENT_TD3 [[Document alloc] initWithWidth: 125.00f andHeight: 88.00f andNumPages: 1]

#define DocumentTD1 DOCUMENT_TD1
#define DocumentTD2 DOCUMENT_TD2
#define DocumentTD3 DOCUMENT_TD3

#define DOCUMENT_MODE_IDDOCUMENT [[NSArray alloc] initWithObjects:DOCUMENT_TD1, DOCUMENT_TD2, nil]
#define DOCUMENT_MODE_PASSPORT   [[NSArray alloc] initWithObjects:DOCUMENT_TD3, nil]
#define DOCUMENT_MODE_ICAO       [[NSArray alloc] initWithObjects:DOCUMENT_TD1, DOCUMENT_TD2, DOCUMENT_TD3, nil]

#define DocumentModeIdDocument DOCUMENT_MODE_IDDOCUMENT
#define DocumentModePassport   DOCUMENT_MODE_PASSPORT
#define DocumentModeICAO       DOCUMENT_MODE_ICAO



/**
 Represents a document type
 */
@interface Document : NSObject {
    @public float width;
    @public float height;
    @public int numPages;
    @public bool isICAO;
    @public double aspectRatio;
}


/**
 Create an instance of a document

 @param p_width width of the document
 @param p_height height of the document
 @return a document instance
 */
-(id)initWithWidth:(float)p_width andHeight:(float)p_height;


/**
 Create an instance of a document

 @param p_width width of the document
 @param p_height height of the document
 @param p_numPages number of pages
 @return a document instance
 */
-(id)initWithWidth:(float)p_width andHeight:(float)p_height andNumPages:(int)p_numPages;


/**
 Compare two documentsw

 @param p_document document to compare
 @return true if the documents are equal
 */
-(BOOL)isEqual:(Document*)p_document;


/**
 Returns an array with TD1 and TD2 documents

 @return array of documents
 */
+ (NSArray<Document *> *) getDocumentModeId;


/**
 Returns an array with TD3 document

 @return array of documents
 */
+ (NSArray<Document *> *) getDocumentModePassport;


/**
 Returns an array of ICAO documents
  - TD1
  - TD2
  - TD3
 
 @return array of documents
 */
+ (NSArray<Document *> *) getDocumentModeICAO;

@end

#endif /* DocType_h */
