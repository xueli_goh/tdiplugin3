package cordova.plugin.tdi;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.ConditionVariable;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import com.aware.face_liveness.api.FaceLiveness;
import com.aware.face_liveness.api.exceptions.FaceLivenessException;

//import com.aware.knomisclient.fragments.ActionButtonFragment;
//import com.aware.knomisclient.fragments.LivenessFragment;
//import com.aware.knomisclient.rest.RestClientTask;

import applicationId.R;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.WRITE_SETTINGS;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONObject;
//import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import android.app.ProgressDialog;

public class CaptureFaceActivity extends AppCompatActivity implements
        FaceLiveness.LivenessActivityPresenter {

    private static CallbackContext callbackContext;

    public static String WORKFLOW = "";
    public static String USERNAME = "";

    private static final String[] REQUESTED_PERMISSIONS = {CAMERA,WRITE_SETTINGS,INTERNET};
    private static final String[] PERMISSIONS = {CAMERA,INTERNET};

    // permission result codes
    private static final int CAMERA_PERM_RESULT = 100;
    private static final int WRITE_SETTINGS_PERM_RESULT = 300;
    private static final int INTERNET_PERM_RESULT = 400;
    private static final int SPLASH_TIME = 3000;

    private static final int ALL_PERMISSIONS_RESULT = 999;
    private static final int CODE_WRITE_SETTINGS_PERMISSION = 1300;  // special case write_settings
    private static int[] REQUESTED_PERMISSION_CODES = {
            CAMERA_PERM_RESULT, WRITE_SETTINGS_PERM_RESULT, INTERNET_PERM_RESULT
    };

    private FaceLiveness mLivenessApi;
    private LivenessFragment fragment = LivenessFragment.newInstance(this);
    private FragmentTransaction ft;

    private boolean oneTimeAsk = false;


    public interface ModelInitializationListener {
        public void onInitializationComplete(boolean success, String modelName);
    }

    private InitializeBackgroundTask mInitializeBackgroundTask;
    private boolean mInitComplete = false;


    private ModelInitializationListener mInitListener = new ModelInitializationListener() {
        @Override
        public void onInitializationComplete(final boolean success, final String modelName) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!success) {
                        Log.e("Could not initialize model", modelName + "not found");
                    }
                }
            });
        }
    };

    public static void initialize(Activity activity, CallbackContext cContext) {//, String faceChoice){
        callbackContext = cContext;
        Intent intent = new Intent(activity, CaptureFaceActivity.class);
        activity.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);  // for camera

        setContentView(R.layout.activity_liveness_custom);
        initializeSDK();
    }

    private void initializeSDK() {

        mInitializeBackgroundTask = new InitializeBackgroundTask(mInitListener);
        ShowHourglass("Initializing");
//        mInitializeBackgroundTask.execute("mobile");
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 1000ms
                mInitializeBackgroundTask.execute("mobile");
            }
        }, 1000);
    }

    @Override
    public void onBackPressed() {
        ShowDialog("Cancel", "Do you want to stop the Capture?");
//        super.onBackPressed();  // fail safe

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void onWorkflowSelected() {

        // Note the following calls must be in the correct order.
        // Must set properties before calling selectWorkflow.
        try {
            mLivenessApi.setProperty(FaceLiveness.PropertyTag.USERNAME, USERNAME);
            mLivenessApi.setProperty(FaceLiveness.PropertyTag.CONSTRUCT_IMAGE, false);
            mLivenessApi.setProperty(FaceLiveness.PropertyTag.TIMEOUT, Double.parseDouble("0"));
            mLivenessApi.setProperty(FaceLiveness.PropertyTag.CAPTURE_ON_DEVICE, true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // select a workflow
        try {
            mLivenessApi.selectWorkflow(this, WORKFLOW, null);
        } catch (Exception e) {
            Log.e("Capture Face Activity", "Invalid workflow setting or incorrect model specified!");

//            checkPermissionsAndSelectItem();
            return;
        }

        HideHourglass();
        launchCameraFragment();
    }

    private void launchCameraFragment() {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_container, fragment, "LivenessFragment");
        ft.addToBackStack("LivenessFragment");
        ft.commit();
    }

    //
    // LivenessPresenter
    //

    public void onCaptureStart() {
        // nothing for now
    }

    public void processResults() {
        String serverPackage = mLivenessApi.getServerPackage();
        byte[] image = mLivenessApi.getCapturedImage();
        String faceImage = android.util.Base64.encodeToString(image, Base64.NO_WRAP);
        String metaData =  android.util.Base64.encodeToString(serverPackage.getBytes(), Base64.NO_WRAP);


                        try {
                            JSONObject jsonObject2 = new JSONObject();
                            jsonObject2.put("faceImage", faceImage);
                            jsonObject2.put("metaData", metaData);
                            callbackContext.success(jsonObject2);
//                            endActivity();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
        endActivity();
    }

    void endActivity()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                closeFragment();
                finish();
            }
        });
    }

    // called when the capture operation is done
    public void onCaptureEnd() {
        processResults();
    }

    // called when the capture operation has timed out
    public void onCaptureTimedout() {
        ShowDialog("Time out", "Face Capture Time out. Do you want to retry?");
    }


    public void onCaptureAbort() {
        ShowDialog("Abort", "Face Capture Abort. Do you want to retry?");
    }

    @Override
    public void onInitializationComplete(final FaceLiveness.InitializationError success) {
        if (success != FaceLiveness.InitializationError.NO_ERROR) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String msg = success.toString() + " contact Aware, Inc.";
                    ShowDialog(getResources().getString(R.string.error), msg);
                    mInitComplete = true;

                }
            });
        } else {
            mInitComplete = true;
            onWorkflowSelected();
        }
    }

    public void ShowDialog(final String title, final String message) {

        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CaptureFaceActivity.this);

                // Setting Dialog Title
                alertDialogBuilder.setTitle(title);

                // set dialog message
                alertDialogBuilder
                        .setMessage(message)
                        .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, close
                                // current activity
                                closeFragment();
                                startActivity(getIntent());
                                finish();
                            }
                        })
                        .setNegativeButton("Stop", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                closeFragment();
                                finish();
                            }
                        })
                        .setCancelable(true)
                ;

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    private void closeFragment() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStackImmediate();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(CaptureFaceActivity.this,PERMISSIONS, ALL_PERMISSIONS_RESULT);
    }

    @Override
    public WeakReference<FaceLiveness> getLivenessComponentApi() {
        if (mLivenessApi == null) {
            return null;
        }
        return new WeakReference<FaceLiveness>(mLivenessApi);
    }

    @SuppressLint("NewApi")
    private boolean checkPermissionsAndSelectItem() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            ArrayList<String> request = findUnAskedPermissions(REQUESTED_PERMISSIONS);

            int code = ALL_PERMISSIONS_RESULT;
            if (request.size() > 0) {
                if (request.size() == 1) {
                    for (int i = 0; i < request.size(); i++) {
                        if (request.get(i).equals(REQUESTED_PERMISSIONS[i])) {
                            code = REQUESTED_PERMISSION_CODES[i];
                            break;
                        }
                    }
                }
//                mNextFragmentToRun = position;
                if (request.contains(WRITE_SETTINGS) && !Settings.System.canWrite(this)) {
                    // we must be >= M if here
//                    mNextFragmentToRun = position;
                    if (!oneTimeAsk) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, CODE_WRITE_SETTINGS_PERMISSION);
                    }
                    return false;
                }
                return true;
            }
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_SETTINGS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_SETTINGS}, CODE_WRITE_SETTINGS_PERMISSION);
            return false;
        }
        return true;
    }

    private boolean hasPermission(String permission) {
        // not using convenience method #usingRuntimePermissions to suppress lint errors
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
    }

    private ArrayList<String> findUnAskedPermissions(String[] wanted) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }
        return result;
    }

    public class InitializeBackgroundTask extends AsyncTask<String, Void, Void> {
        private boolean mCouldNotOpenModel = false;
        private String mModelName = "";
        private ModelInitializationListener mInitializationListener;

        InitializeBackgroundTask(ModelInitializationListener listener) {
            mInitializationListener = listener;
            mCouldNotOpenModel = false;
            mModelName = "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(final String[] params) {
            mModelName = params[0];
            try {
                FaceLiveness.setStaticProperty(FaceLiveness.StaticPropertyTag.FACE_MODEL, params[0]);
                mLivenessApi = new FaceLiveness(getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                // initialize the library, wait for callback...
                mLivenessApi.initializeFaceLivenessLibrary(CaptureFaceActivity.this);
            } catch (Exception e) {
                mCouldNotOpenModel = true;
                return null;
            }

            synchronized (InitializeBackgroundTask.this) {
                while (!mInitComplete && !isCancelled()) {
                    try {
                        Log.i("Initialize Background Task", "Launch starting WAIT");
                        InitializeBackgroundTask.this.wait(SPLASH_TIME);
                        Log.i("Initialize Background Task", "Launch completing WAIT");
                    } catch (Exception e) {
                        Log.e("Initialize Background Task", "Launch INTERRUPTED: " + e.getMessage());
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void o) {
            super.onPostExecute(o);
            if (isCancelled()) {
                finish();  // ends the Launch Activity
            }
//            HideHourglass();
            mInitializationListener.onInitializationComplete(!mCouldNotOpenModel, mModelName);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        mNextFragmentToRun = NO_FRAGMENT_TO_RUN;

        if(requestCode == ALL_PERMISSIONS_RESULT){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                initializeSDK();
            }else {
                Toast.makeText(CaptureFaceActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    ProgressDialog hourglass;

    public void ShowHourglass()
    {
        runOnUiThread(new Runnable() {
            public void run() {
                hourglass = new ProgressDialog(CaptureFaceActivity.this);
                hourglass.setMessage("Processing...");
                hourglass.setIndeterminate(true);
                hourglass.setCancelable(false);
                hourglass.show();
            }
        });
    }


    public void ShowHourglassDirect()
    {
        hourglass = new ProgressDialog(CaptureFaceActivity.this);
        hourglass.setMessage("Processing...");
        hourglass.setIndeterminate(true);
        hourglass.setCancelable(false);
        hourglass.show();
    }

    public void ShowHourglass(final String message)
    {
        runOnUiThread(new Runnable() {
            public void run() {
                hourglass = new ProgressDialog(CaptureFaceActivity.this);
                hourglass.setMessage(message);
                hourglass.setIndeterminate(true);
                hourglass.setCancelable(false);
                hourglass.show();
            }
        });
    }

    public void HideHourglass()
    {
        runOnUiThread(new Runnable() {
            public void run() {
                if (hourglass != null) {
                    hourglass.dismiss();
                    hourglass = null;
                }
            }
        });
    }

//    void HideKeyboard()
//    {
//        View view = this.getCurrentFocus();
//        if (view != null) {
//            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//        }
//    }
}
