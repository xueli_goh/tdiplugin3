//
//  McidPlugin.h
//  SampleApp
//
//  Created by Luciana Silva Daré on 28/03/19.
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

/**
 * GAH cordova plugin wrapping the IDV SDK.
 */
@interface TdiPlugin : CDVPlugin

/**
 * Initializes IDV SDK.
 */
- (void)init:(CDVInvokedUrlCommand*)command;
- (void)setDetectionZone:(CDVInvokedUrlCommand*)command;
- (void)setCaptureDocuments:(CDVInvokedUrlCommand*)command;
- (void)setCaptureNewDocument:(CDVInvokedUrlCommand*)command;
- (void)setAutoSnapshot:(CDVInvokedUrlCommand*)command;
- (void)setEdgeDetectionTimeout:(CDVInvokedUrlCommand*)command;
- (void)setAutocropping:(CDVInvokedUrlCommand*)command;
- (void)hideUIElementsForStep:(CDVInvokedUrlCommand*)command;
- (void)setQACheckResultTimeout:(CDVInvokedUrlCommand*)command;
- (void)setSecondPageTimeout:(CDVInvokedUrlCommand*)command;
- (void)initFaceCapture:(CDVInvokedUrlCommand*)command;
- (void)initTDI:(CDVInvokedUrlCommand*)command;
- (void)setTDIConfig:(CDVInvokedUrlCommand*)command;
- (void)setLivenessConfig:(CDVInvokedUrlCommand*)command;
- (void)setTextColor:(CDVInvokedUrlCommand*)command;
- (void)setBackgroundColor:(CDVInvokedUrlCommand*)command;
- (void)setTextType:(CDVInvokedUrlCommand*)command;
- (void)setFeedbackBackgroundColor:(CDVInvokedUrlCommand*)command;
@end
