//
//  Store.h
//  SampleApp
//
//  Created by Luciana Silva Daré on 28/03/19.
//

#ifndef Store_h
#define Store_h

#import <Foundation/Foundation.h>
#import <IDV_Doc/CaptureInterface.h>

@interface Store : NSObject
+(Store*) getInstance;

@property NSUserDefaults* preferences;

-(void) addValue:(CaptureError)value forKey:(NSString*)key;
-(NSInteger) getValueForKey:(NSString*)key;
@end
#endif /* Store_h */
